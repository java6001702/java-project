package epsi.software.sakila.service;

import epsi.software.sakila.entities.City;


import java.util.List;
/**
 * Interface - Crud - City
 */
public interface CityService {
    City create(City city);

    City read(long id);

    List<City> readAll();

    City delete(City city);

    City update(Long id);
}

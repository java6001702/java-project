package epsi.software.sakila.service;

import epsi.software.sakila.entities.Country;

import java.util.List;
/**
 * Interface - Crud - Country
 */
public interface CountryService {
    Country create(Country country);

    Country read(long id);

    List<Country> readAll();

    Country delete(Country country);

    Country update(Long id);
}
